<?php
/**
 * Created by PhpStorm.
 * User: wanghui
 * Date: 2019/02/26
 * Time: 3:33 PM
 */

namespace App\Http\Controllers\V1;

use App\Models\Address;
use App\Models\Cart;
use App\Models\Order;
use App\Models\OrderInvoice;
use App\Models\Payment;
use App\Models\Seller;
use App\Services\GoodsService;
use App\Services\OrderService;
use Illuminate\Http\Request;

class OrderController extends BaseController
{
    /**
     * 订单金额计算
     * @param Request $request
     * @return array
     * @throws \App\Exceptions\ApiException
     */
    public function getPrice(Request $request)
    {
        $m_id = $this->getUserId();
        $type = $request->post('type');
        $sku_id = $request->post('sku_id');
        $buy_qty = (int)$request->post('buy_qty');
        $address_id = (int)$request->post('address_id');
        if (!$type || !$sku_id || !$address_id) {
            api_error(__('api.missing_params'));
        }
        //格式化优惠券配送备注发票等信息
        list($coupons, $delivery, $note, $invoice) = $this->formatParams($request);
        $cart = self::getCart($type, $sku_id, $buy_qty);
        if (!$cart) {
            api_error(__('api.goods_error'));
        }
        //验证地址是否存在
        $prov_id = 0;
        $address = Address::where(['m_id' => $m_id, 'id' => $address_id])->first();
        if ($address) {
            $prov_id = $address['prov_id'];
        } else {
            api_error(__('api.address_not_exists'));
        }

        $seller_goods = GoodsService::getOrderPrice($cart, $prov_id, $coupons);//获取商品信息
        $price = GoodsService::sumOrderPrice($seller_goods);//组装价格信息
        return $this->success($price);
    }

    /**
     * 确认订单
     * @param Request $request
     * @return array
     * @throws \App\Exceptions\ApiException
     */
    public function confirm(Request $request)
    {
        $m_id = $this->getUserId();
        $type = $request->post('type');
        $sku_id = $request->post('sku_id');
        $buy_qty = (int)$request->post('buy_qty');
        $address_id = (int)$request->post('address_id');
        if (!$type || !$sku_id) {
            api_error(__('api.missing_params'));
        }
        $cart = self::getCart($type, $sku_id, $buy_qty);
        if (!$cart) {
            api_error(__('api.goods_error'));
        }
        //验证地址是否存在
        $prov_id = 0;
        $address = array();
        if (!$address_id) {
            //没有收货地址查询默认地址
            $res_address = Address::where(['m_id' => $m_id])->orderBy('default', 'desc')->orderBy('id', 'desc')->first();
        } else {
            $res_address = Address::where(['m_id' => $m_id, 'id' => $address_id])->first();
        }
        if ($res_address) {
            $prov_id = $res_address['prov_id'];
            $address = array(
                'id' => $res_address['id'],
                'full_name' => $res_address['full_name'],
                'tel' => $res_address['tel'],
                'address' => $res_address['prov_name'] . $res_address['city_name'] . $res_address['area_name'] . $res_address['default']
            );
        }
        $seller_goods = GoodsService::getConfirm($cart, $prov_id);

        $order_price = GoodsService::sumOrderPrice($seller_goods);//组装价格信息
        $return = array(
            'address' => $address,
            'seller_goods' => $seller_goods,
            'order_price' => $order_price,
            'payment' => Payment::getPayment()
        );
        return $this->success($return);
    }

    /**
     * 提交订单
     * @param Request $request
     * @return array
     * @throws \App\Exceptions\ApiException
     */
    public function submit(Request $request)
    {
        $m_id = $this->getUserId();
        $type = $request->post('type');
        $sku_id = $request->post('sku_id');
        $buy_qty = (int)$request->post('buy_qty');
        $address_id = (int)$request->post('address_id');
        if (!$type || !$sku_id || !$address_id) {
            api_error(__('api.missing_params'));
        }
        //格式化优惠券配送备注发票等信息
        list($coupons, $delivery, $note, $invoice) = $this->formatParams($request);

        $cart = self::getCart($type, $sku_id, $buy_qty);
        if (!$cart) {
            api_error(__('api.goods_error'));
        }
        //验证地址是否存在
        $prov_id = 0;
        $address = Address::where(['m_id' => $m_id, 'id' => $address_id])->first();
        if (!$address) {
            api_error(__('api.address_not_exists'));
        }

        $seller_goods = GoodsService::getOrderPrice($cart, $address['prov_id'], $coupons);//获取商品信息

        //判断是否存在不能送达的商品
        foreach ($seller_goods as $value) {
            if ($value['delivery']['sku_ids']) {
                api_error(__('api.goods_can_not_delivery'));
            }
        }
        //检测发票信息
        $invoice = self::checkInvoice($seller_goods, $invoice);

        //开始组装订单信息
        $order_info = array();
        $order_no_arr = array();
        foreach ($seller_goods as $value) {
            $seller_id = $value['seller']['id'];
            $order_no = OrderService::getOrderNo();
            $order_no_arr[] = $order_no;
            $_order = array(
                'm_id' => $m_id,
                'seller_id' => $seller_id,
                'order_no' => $order_no,
                'status' => Order::STATUS_WAIT_PAY,
                'product_num' => $value['all_buy_qty'],
                'sell_price_total' => $value['price']['sell_price'],
                'market_price_total' => $value['price']['market_price'],
                'delivery_type' => isset($delivery[$seller_id]['delivery_type']) ? $delivery[$seller_id]['delivery_type'] : 1,
                'delivery_time' => isset($delivery[$seller_id]['delivery_time']) ? $delivery[$seller_id]['delivery_time'] : '',
                'delivery_price' => $value['delivery']['delivery_price'],
                'delivery_price_real' => $value['delivery']['delivery_price_real'],
                'promotion_price' => $value['price']['promotion_price'],
                'promotion_text' => isset($value['promotion']) ? json_encode($value['promotion'], JSON_UNESCAPED_UNICODE) : '',
                'subtotal' => $value['price']['subtotal'],
                'coupons_id' => isset($value['coupons_id']) ? $value['coupons_id'] : 0,
                'platform' => get_platform(),
                'full_name' => $address['full_name'],
                'tel' => $address['tel'],
                'prov' => $address['prov_name'],
                'city' => $address['city_name'],
                'area' => $address['area_name'],
                'address' => $address['address'],
                'note' => isset($note[$seller_id]) ? $note[$seller_id] : '',
            );

            //发票信息
            if ($invoice) {
                $_order['invoice'] = array(
                    'type' => $invoice[$seller_id]['type'],
                    'title' => $invoice[$seller_id]['title'],
                    'tax_no' => isset($invoice[$seller_id]['tax_no']) ? $invoice[$seller_id]['tax_no'] : ''
                );
            }

            $order_goods = array();
            foreach ($value['goods'] as $goods) {
                $_order_goods = array(
                    'm_id' => $m_id,
                    'goods_id' => $goods['goods_id'],
                    'goods_title' => $goods['title'],
                    'sku_id' => $goods['sku_id'],
                    'sku_code' => $goods['sku_code'],
                    'image' => $goods['image'],
                    'sell_price' => $goods['show_price'],
                    'market_price' => $goods['line_price'],
                    'promotion_price' => $goods['promotion_price'],
                    'buy_qty' => $goods['buy_qty'],
                    'weight' => $goods['weight'] * $goods['buy_qty'],
                    'spec_value' => $goods['spec_value'],
                    'seller_id' => $seller_id,
                );
                $order_goods[] = $_order_goods;
            }
            $_order['goods'] = $order_goods;
            $order_info[] = $_order;
        }
        $res = Order::submitOrder($order_info);
        if ($res) {
            //减少商品库存
            OrderService::stockDecr($cart);
            //删除购物车商品
            self::delCart($type, $sku_id);

            $order_no_arr = join(',', $order_no_arr);
            $return = array(
                'order_no' => trim($order_no_arr, ',')
            );
            return $this->success($return);
        } else {
            api_error(__('api.order_submit_fail'));
        }
    }

    /**
     * 组装商品购买信息
     * @param $type
     * @param $sku_id
     * @param $buy_qty
     * @throws \App\Exceptions\ApiException
     */
    private function getCart($type, $sku_id, $buy_qty)
    {
        $m_id = $this->getUserId();
        //购物车提交
        $cart = array();
        if ($type == 1) {
            $sku_id = format_number($sku_id, true);
            if (!$sku_id) {
                api_error(__('api.missing_params'));
            }
            $cart = Cart::where('m_id', $m_id)->whereIn('sku_id', $sku_id)->orderBy('updated_at', 'desc')->pluck('buy_qty', 'sku_id');
            if ($cart->isEmpty()) {
                api_error(__('api.cart_goods_not_exists'));
            }
            $cart = $cart->toArray();
        } elseif ($type == 2) {
            //直接购买
            $sku_id = (int)$sku_id;
            if (!$sku_id) {
                api_error(__('api.goods_error'));
            }
            if ($buy_qty < 1) {
                api_error(__('api.buy_qty_error'));
            }
            $cart = array($sku_id => $buy_qty);
        }
        return $cart;
    }

    /**
     * 删除购物车商品
     * @param $type
     * @param $sku_id
     * @param $buy_qty
     * @throws \App\Exceptions\ApiException
     */
    private function delCart($type, $sku_id)
    {
        $m_id = $this->getUserId();
        //购物车提交
        if ($type == 1) {
            $sku_id = format_number($sku_id, true);
            if (is_array($sku_id)) {
                $cart = Cart::where('m_id', $m_id)->whereIn('sku_id', $sku_id)->delete();
            } else {
                $cart = Cart::where(['m_id' => $m_id, 'sku_id' => $sku_id])->delete();
            }
        }
    }

    /**
     * 格式化参数
     * @param string $params
     */
    private function formatParams(Request $request)
    {
        $coupons = $request->input('coupons');
        if ($coupons) $coupons = json_decode($coupons, true);

        $note = $request->input('note');
        if ($note) $note = json_decode($note, true);

        $delivery = $request->input('delivery');
        if ($delivery) $delivery = json_decode($delivery, true);

        $invoice = $request->input('invoice');
        if ($invoice) $invoice = json_decode($invoice, true);

        return [$coupons, $delivery, $note, $invoice];
    }

    /**
     * 验证发票信息
     * @param $seller_goods
     * @param $invoice
     * @return mixed
     * @throws \App\Exceptions\ApiException
     */
    private function checkInvoice($seller_goods, $invoice)
    {
        foreach ($seller_goods as $value) {
            $seller_id = $value['seller']['id'];
            if ($invoice[$seller_id]) {
                $_seller_invoice = $invoice[$seller_id];
                if ($value['seller']['invoice'] == Seller::INVOICE_ON) {
                    if (!isset($_seller_invoice['title']) || !$_seller_invoice['title']) {
                        api_error(__('api.invoice_title_error'));
                    }
                    if (!isset(OrderInvoice::TYPE_DESC[$_seller_invoice['type']])) {
                        api_error(__('api.invalid_params'));
                    }
                    if ($_seller_invoice['type'] == OrderInvoice::TYPE_ENTERPRISE && !$_seller_invoice['tax_no']) {
                        api_error(__('api.invoice_tax_no_error'));
                    }
                } else {
                    unset($invoice[$seller_id]);
                }
            }
        }
        return $invoice;
    }
}
