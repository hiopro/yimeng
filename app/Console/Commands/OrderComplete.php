<?php

namespace App\Console\Commands;

use App\Models\Order;
use App\Models\OrderGoods;
use App\Models\OrderLog;
use App\Models\Refund;
use App\Models\Seller;
use App\Models\SellerBalance;
use App\Models\SellerBalanceDetail;
use App\Services\OrderService;
use Illuminate\Console\Command;

class OrderComplete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order_complete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '订单交易完成';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $page = 1;
        $pagesize = 10;
        $user_data = array(
            'id' => 0,
            'username' => 'system'
        );
        $where = array(
            ['status', Order::STATUS_COMMENT],
            ['comment_at', '<', get_date(time() - config('other.order.order_complete_time'))]
        );
        while (true) {
            $offset = ($page - 1) * $pagesize;
            $order_res = Order::select('id', 'm_id', 'status', 'seller_id', 'subtotal', 'order_no')->where($where)->offset($offset)->limit($pagesize)->orderBy('id', 'asc')->get();
            if ($order_res->isEmpty()) {
                break;
            } else {
                $page++;
                $order_ids = $seller_ids = array();
                foreach ($order_res as $order) {
                    $order_ids[] = $order['id'];
                    $seller_ids[] = $order['seller_id'];
                }
                //查询正在售后的订单
                $refund_order_id = OrderGoods::whereIn('order_id', array_unique($order_ids))->whereIn('refund', [OrderGoods::REFUND_APPLY, OrderGoods::REFUND_ONGOING])->pluck('order_id')->toArray();

                //组合没有在售后的订单
                $new_order_ids = array();
                foreach ($order_res as $order) {
                    if (!in_array($order['id'], $refund_order_id)) {
                        $new_order_ids[] = $order['id'];
                    }
                }
                //查询已经完成的售后单
                $refund = array();
                $refund_res = Refund::select('order_id', 'amount')->whereIn('order_id', array_unique($new_order_ids))->where('status', Refund::STATUS_DONE)->get();
                if (!$refund_res->isEmpty()) {
                    foreach ($refund_res as $value) {
                        if (!isset($refund[$value['order_id']])) $refund[$value['order_id']] = 0;
                        $refund[$value['order_id']] += $value['amount'];
                    }
                }

                //查询商家结算信息
                $seller = Seller::whereIn('id', array_unique($seller_ids))->pluck('pct', 'id');

                //修改订单状态
                Order::whereIn('id', $new_order_ids)->where('status', Order::STATUS_COMMENT)->update(['status' => Order::STATUS_COMPLETE, 'complete_at' => get_date()]);
                //开始结算
                foreach ($order_res as $order) {
                    if (in_array($order['id'], $new_order_ids)) {
                        $refund_amount = isset($refund[$order['id']]) ? $refund[$order['id']] : 0;//售后金额
                        $pct = isset($seller[$order['seller_id']]) ? $seller[$order['seller_id']] : 0;//商家结算手续费比例
                        $amount = format_price($order['subtotal'] - $refund_amount);//待结算金额
                        $poundage = format_price($amount * ($pct / 100));
                        SellerBalance::updateAmount($order['seller_id'], $amount, SellerBalanceDetail::EVENT_ORDER, $order['order_no'], '订单完成结算');
                        if ($poundage) {
                            SellerBalance::updateAmount($order['seller_id'], -$poundage, SellerBalanceDetail::EVENT_POUNDAGE, $order['order_no'], '订单结算手续费');
                        }
                    }
                }
            }
        }
    }
}
