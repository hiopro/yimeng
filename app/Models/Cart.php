<?php
/**
 * Created by PhpStorm.
 * User: wanghui
 * Date: 2018/5/11
 * Time: 下午4:46
 */

namespace App\Models;

/**
 * 购物车
 * Class Adv
 * @package App\Models
 */
class Cart extends BaseModels
{

    protected $table = 'cart';
    protected $guarded = ['id'];

}
