<?php
/**
 * Created by PhpStorm.
 * User: wanghui
 * Date: 2018/5/9
 * Time: 上午11:14
 */

namespace App\Libs\Weixin;

use EasyWeChat\Factory;

/**
 * 公众号
 * Class Sms
 * @package App\Libs
 */
class Mp
{

    private $app;

    function __construct()
    {
        $config = [
            'app_id' => config('weixin.mp.appid'),
            'secret' => config('weixin.mp.secret'),
            // 下面为可选项
            // 指定 API 调用返回结果的类型：array(default)/collection/object/raw/自定义类名
            'response_type' => 'array',
        ];

        $this->app = Factory::officialAccount($config);
    }

    /**
     * 获取配置好的实例信息
     * @return \EasyWeChat\OfficialAccount\Application
     */
    public function getApp()
    {
        return $this->app;
    }

    /**
     * 获取微信授权后的用户信息
     * @param $code
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     */
    public function userInfo()
    {
        try{
            $user = $this->app->oauth->user();
            $auth_info = $user->getOriginal();
            return $auth_info;
        } catch (\Exception $e) {
            return false;
        }
    }
}